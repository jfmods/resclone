plugins {
    id("jfmod") version "1.6-SNAPSHOT"
}

loom {
    accessWidenerPath.set(file("src/main/resources/resclone.accesswidener"))
}

allprojects { group = "io.gitlab.jfronny" }
base.archivesName = "resclone"

val modmenuVersion = "12.0.0-beta.1"
val commonsVersion = "1.8.0-SNAPSHOT"
jfMod {
    minecraftVersion = "1.21.4"
    yarn("build.1")
    loaderVersion = "0.16.9"
    libJfVersion = "3.18.3"
    fabricApiVersion = "0.110.5+1.21.4"

    modrinth {
        projectId = "resclone"
        optionalDependencies.add("fabric-api")
    }
    curseforge {
        projectId = "839008"
        optionalDependencies.add("fabric-api")
    }
}

dependencies {
    include(modImplementation("io.gitlab.jfronny.libjf:libjf-base")!!) // for JfCommons
    include(modImplementation("io.gitlab.jfronny.libjf:libjf-config-core-v2")!!) // for JfCommons
    include(modImplementation("net.fabricmc.fabric-api:fabric-resource-loader-v0")!!)

    compileOnly("io.gitlab.jfronny:commons-serialize-generator-annotations:$commonsVersion")
    annotationProcessor("io.gitlab.jfronny:commons-serialize-generator:$commonsVersion")

    // Dev env
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-config-ui-tiny")
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil")
    modLocalRuntime("com.terraformersmc:modmenu:$modmenuVersion")
    // for modmenu
    modLocalRuntime("net.fabricmc.fabric-api:fabric-resource-loader-v0")
    modLocalRuntime("net.fabricmc.fabric-api:fabric-screen-api-v1")
    modLocalRuntime("net.fabricmc.fabric-api:fabric-key-binding-api-v1")
}
