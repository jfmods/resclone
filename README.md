Resclone automatically downloads and updates resource and data packs specified in its config.
Packs are updated on startup whenever possible, meaning that very large packs will slow down your startup.

Example config:
```
{
  // The packs to be loaded by resclone
  "packs": [
    {
      "fetcher": "file",
      "source": "https://github.com/FaithfulTeam/Faithful/raw/releases/1.16.zip",
      "name": "Faithful",
      "forceDownload": false,
      "forceEnable": false
    },
    {
      "fetcher": "github",
      "source": "seasnail8169/SnailPack",
      "name": "SnailPack",
      "forceDownload": false,
      "forceEnable": false
    },
    {
      "fetcher": "github",
      "source": "spiralhalo/LumiLights/release",
      "name": "LumiLights",
      "forceDownload": true,
      "forceEnable": false
    },
    {
      "fetcher": "github",
      "source": "spiralhalo/LumiPBRExt/tag/v0.7",
      "name": "LumiPBRExt",
      "forceDownload": false,
      "forceEnable": false
    },
    {
      "fetcher": "github",
      "source": "FaithfulTeam/Faithful/branch/1.16",
      "name": "Faithful",
      "forceDownload": false,
      "forceEnable": false
    },
    {
      "fetcher": "file",
      "source": "https://media.forgecdn.net/files/3031/178/BattyCoordinates_005.zip",
      "name": "Battys Coordinates",
      "forceDownload": false,
      "forceEnable": false
    },
    {
      "fetcher": "curseforge",
      "source": "325017",
      "name": "Vanilla Additions",
      "forceDownload": false,
      "forceEnable": true
    },
    {
      "fetcher": "modrinth",
      "source": "new-in-town",
      "name": "New in Town"
    }
  ],
  // Whether to prune unused packs from the cache
  "pruneUnused": true
}
```