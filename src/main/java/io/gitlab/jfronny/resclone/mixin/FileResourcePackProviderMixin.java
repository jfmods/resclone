package io.gitlab.jfronny.resclone.mixin;

import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.RescloneResourcePack;
import io.gitlab.jfronny.resclone.data.PackMetaLoaded;
import net.minecraft.resource.*;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Optional;
import java.util.function.Consumer;

@Mixin(FileResourcePackProvider.class)
public class FileResourcePackProviderMixin {
    @Shadow @Final private ResourcePackSource source;
    @Shadow @Final private ResourceType type;

    @Inject(at = @At("TAIL"), method = "register(Ljava/util/function/Consumer;)V")
    public void registerExtra(Consumer<ResourcePackProfile> consumer, CallbackInfo info) {
        for (PackMetaLoaded meta : Resclone.DOWNLOADED_PACKS) {
            ResourcePackInfo ifo = new ResourcePackInfo(
                "resclone/" + meta.name(),
                Text.literal(meta.name()),
                source,
                Optional.empty()
            );
            ResourcePackProfile resourcePackProfile = ResourcePackProfile.create(
                    ifo,
                    new RescloneResourcePack.Factory(meta.zipPath().toFile(), ifo),
                    type,
                    new ResourcePackPosition(
                            meta.forceEnable(),
                            ResourcePackProfile.InsertionPosition.TOP,
                            false
                    )
            );
            if (resourcePackProfile != null) {
                consumer.accept(resourcePackProfile);
            }
        }
    }
}
