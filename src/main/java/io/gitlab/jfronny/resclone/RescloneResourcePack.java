package io.gitlab.jfronny.resclone;

import net.fabricmc.fabric.api.resource.ModResourcePack;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.resource.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RescloneResourcePack extends ZipResourcePack implements ModResourcePack {
    private static final ModMetadata METADATA = FabricLoader.getInstance().getModContainer(Resclone.MOD_ID).orElseThrow().getMetadata();
    private final ResourcePackInfo info;
    private final ZipFileWrapper file;

    RescloneResourcePack(ZipFileWrapper file, ResourcePackInfo info, String overlay) {
        super(info, file, overlay);
        this.info = info;
        this.file = file;
    }

    @Override
    public ModMetadata getFabricModMetadata() {
        return METADATA;
    }

    @Override
    public ModResourcePack createOverlay(String overlay) {
        return new RescloneResourcePack(this.file, this.info, overlay);
    }

    public static class Factory implements ResourcePackProfile.PackFactory {
        private final File file;
        private final ResourcePackInfo info;

        public Factory(File file, ResourcePackInfo info) {
            this.file = file;
            this.info = info;
        }

        @Override
        public ResourcePack open(ResourcePackInfo info) {
            ZipFileWrapper zipFileWrapper = new ZipFileWrapper(this.file);
            return new RescloneResourcePack(zipFileWrapper, this.info, "");
        }

        @Override
        public ResourcePack openWithOverlays(ResourcePackInfo info, ResourcePackProfile.Metadata metadata) {
            ZipFileWrapper zipFileWrapper = new ZipFileWrapper(this.file);
            ZipResourcePack resourcePack = new RescloneResourcePack(zipFileWrapper, this.info, "");
            List<String> overlays = metadata.overlays();
            if (overlays.isEmpty()) return resourcePack;
            List<ResourcePack> overlayPacks = new ArrayList<>(overlays.size());
            for (String string : overlays) overlayPacks.add(new RescloneResourcePack(zipFileWrapper, this.info, string));
            return new OverlayResourcePack(resourcePack, overlayPacks);
        }
    }
}
