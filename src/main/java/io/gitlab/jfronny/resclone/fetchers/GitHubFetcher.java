package io.gitlab.jfronny.resclone.fetchers;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.data.github.GC_Release;
import io.gitlab.jfronny.resclone.data.github.GC_Repository;
import io.gitlab.jfronny.resclone.data.github.Release;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;

public class GitHubFetcher extends BasePackFetcher {
    @Override
    public String getSourceTypeName() {
        return "github";
    }

    @Override
    String getDownloadUrl(String baseUrl) throws Exception {
        String[] parts = baseUrl.split("/");

        if (parts.length < 2) {
            throw new Exception("Minimum source must contain \"user/repo\".");
        }

        //"user/repo" - Gets from latest commit of main/master branch.
        else if (parts.length == 2) {
            return getFromBranch(baseUrl, null);
        }

        //"user/repo/branch/branchName" - Gets from latest commit of specified branch.
        else if (parts[2].equalsIgnoreCase("branch")) {
            if (parts.length < 4) throw new Exception("Missing branch name in source definition.");
            else {
                Resclone.LOGGER.info("Getting from " + parts[3] + " branch.");
                return getFromBranch(parts[0] + "/" + parts[1], parts[3]);
            }
        }

        //"user/repo/release" - Gets from the latest release.
        else if (parts[2].equalsIgnoreCase("release")) {
            try (Reader r = HttpClient.get("https://api.github.com/repos/" + parts[0] + "/" + parts[1] + "/releases/latest").sendReader();
                 JsonReader jr = LibJf.LENIENT_TRANSPORT.createReader(r)) {
                Release latestRelease = GC_Release.deserialize(jr);
                String res = null;

                for (Release.Asset asset : latestRelease.assets) {
                    if ("application/x-zip-compressed".equals(asset.content_type) || asset.name.endsWith(".zip")) {
                        res = asset.browser_download_url;
                        break;
                    }
                }

                Resclone.LOGGER.info("Getting from latest release.");

                if (res == null) return latestRelease.zipball_url;
                else return res;

            } catch (Throwable e) {
                throw new Exception("Failed to get github release asset", e);
            }
        }

        //"user/repo/tag/tagNum" - Gets from a specified tag.
        else if (parts[2].equalsIgnoreCase("tag")) {
            if (parts.length < 4) throw new Exception("Missing tag number in source definition.");
            else {
                return getFromTag(parts[0] + "/" + parts[1], parts[3]);
            }
        }

        return null;
    }

    private String getFromBranch(String repo, @Nullable String branch) {
        if (branch == null) {
            try (Reader r = HttpClient.get("https://api.github.com/repos/" + repo).sendReader();
                 JsonReader jr = LibJf.LENIENT_TRANSPORT.createReader(r)) {
                branch = GC_Repository.deserialize(jr).default_branch;
            } catch (IOException | URISyntaxException e) {
                Resclone.LOGGER.error("Failed to fetch branch for " + repo + ". Choosing \"main\"", e);
                branch = "main";
            }
        }
        Resclone.LOGGER.info("Getting " + repo + " from " + branch + " branch.");
        return "https://codeload.github.com/" + repo + "/legacy.zip/refs/heads/" + branch;
    }

    private String getFromTag(String repo, String tag) {
        Resclone.LOGGER.info("Getting from tag " + tag + ".");
        return "https://codeload.github.com/" + repo + "/legacy.zip/refs/tags/" + tag;
    }
}
