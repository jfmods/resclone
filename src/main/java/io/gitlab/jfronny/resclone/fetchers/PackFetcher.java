package io.gitlab.jfronny.resclone.fetchers;

import java.nio.file.Path;

public interface PackFetcher {
    Result get(String baseUrl, Path targetDir, boolean forceDownload) throws Exception;
    String getSourceTypeName(); // The name for users to specify in the config

    record Result(Path downloadPath, boolean freshDownload) { }
}
