package io.gitlab.jfronny.resclone.fetchers;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.resclone.Resclone;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class BasePackFetcher implements PackFetcher {
    abstract String getDownloadUrl(String baseUrl) throws Exception; // Return the actual download URL for the file based on the provided string

    public Result get(String baseUrl, Path targetDir, boolean forceDownload) throws Exception {
        String url;
        try {
            url = getDownloadUrl(baseUrl);
            Resclone.urlCache.set(baseUrl, url);
        } catch (Exception e) {
            if (Resclone.urlCache.containsKey(baseUrl)) {
                Resclone.LOGGER.error("Could not get download URL for " + baseUrl + ", using cached", e);
                url = Resclone.urlCache.get(baseUrl);
            } else {
                throw e;
            }
        }
        Path p = targetDir.resolve(Integer.toHexString(url.hashCode()));

        if (!forceDownload && Files.exists(p)) {
            Resclone.packCount++;
            return new Result(p, false);
        }

        Resclone.LOGGER.info("Downloading pack: " + url);

        try (InputStream is = HttpClient.get(url).userAgent(Resclone.USER_AGENT).sendInputStream();
             OutputStream os = Files.newOutputStream(p)) {
            byte[] dataBuffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(dataBuffer, 0, 1024)) != -1) {
                os.write(dataBuffer, 0, bytesRead);
            }
            Resclone.LOGGER.info("Finished downloading.");
        } catch (Throwable e) {
            throw new IOException("Could not download pack", e);
        }
        Resclone.packCount++;
        return new Result(p, true);
    }
}
