package io.gitlab.jfronny.resclone.fetchers;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.data.modrinth.GC_Version;
import io.gitlab.jfronny.resclone.data.modrinth.Version;
import io.gitlab.jfronny.resclone.util.ListAdaptation;
import net.minecraft.MinecraftVersion;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.util.Date;
import java.util.List;

public class ModrinthFetcher extends BasePackFetcher {
    @Override
    public String getSourceTypeName() {
        return "modrinth";
    }

    @Override
    public String getDownloadUrl(String baseUrl) throws Exception {
        try {
            String version = MinecraftVersion.CURRENT.getName();

            Version latest = null;
            Date latestDate = null;
            boolean foundMatchingVersion = false;

            List<Version> versions;
            try (Reader r = HttpClient.get("https://api.modrinth.com/v2/project/" + baseUrl + "/version")
                    .userAgent(Resclone.USER_AGENT)
                    .sendReader();
                 JsonReader jr = LibJf.LENIENT_TRANSPORT.createReader(r)) {
                versions = ListAdaptation.deserializeList(jr, GC_Version::deserialize);
            }
            for (Version ver : versions) {
                if (foundMatchingVersion && !ver.game_versions.contains(version)) continue;
                if (ver.files.isEmpty()) continue;
                if (!foundMatchingVersion && ver.game_versions.contains(version)) {
                    foundMatchingVersion = true;
                    latest = null;
                }
                if (latest == null || ver.date_published.after(latestDate)) {
                    latest = ver;
                    latestDate = ver.date_published;
                }
            }

            if (latest == null) throw new FileNotFoundException("Could not identify valid version");
            if (!foundMatchingVersion) Resclone.LOGGER.error("Could not find matching version of " + baseUrl + ", using latest");

            for (Version.File file : latest.files) {
                if (file.primary) return file.url;
            }
            Resclone.LOGGER.error("Could not identify primary file of " + baseUrl + ", attempting identification by file_type");
            for (Version.File file : latest.files) {
                if (file.file_type == Version.File.Type.REQUIRED_RESOURCE_PACK) return file.url;
            }
            Resclone.LOGGER.error("Identification failed, using first file of " + baseUrl);
            return latest.files.getFirst().url;
        } catch (Throwable e) {
            throw new IOException("Could not get Modrinth download for " + baseUrl, e);
        }
    }
}
