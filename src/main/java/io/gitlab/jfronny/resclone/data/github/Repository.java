package io.gitlab.jfronny.resclone.data.github;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

@GSerializable
public class Repository {
    public String default_branch;
}
