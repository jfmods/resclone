package io.gitlab.jfronny.resclone.data.curseforge;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.Date;
import java.util.List;

@GSerializable
public class GetModFilesResponse {
    public List<Data> data;

    @GSerializable
    public static class Data {
        public String downloadUrl;
        public Date fileDate;
        public List<String> gameVersions;
    }
}
