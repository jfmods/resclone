package io.gitlab.jfronny.resclone.data.curseforge;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

@GSerializable
public class GetModResponse {
    public Data data;

    @GSerializable
    public static class Data {
        public Boolean allowModDistribution;
    }
}