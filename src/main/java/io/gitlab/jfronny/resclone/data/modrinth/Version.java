package io.gitlab.jfronny.resclone.data.modrinth;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.List;

@GSerializable
public class Version {
    public String name;
    public String version_number;
    @Nullable public String changelog;
    public List<Dependency> dependencies;
    public List<String> game_versions;
    public VersionType version_type;
    public List<String> loaders;
    public Boolean featured;
    public Status status;
    @Nullable public Status requested_status;
    public String id;
    public String project_id;
    public String author_id;
    public Date date_published;
    public Integer downloads;
    public List<File> files;

    @GSerializable
    public static class Dependency {
        @Nullable public String version_id;
        @Nullable public String project_id;
        @Nullable public String file_name;
        public Type dependency_type;

        public enum Type {
            required, optional, incompatible, embedded
        }
    }

    public enum VersionType {
        release, beta, alpha
    }

    public enum Status {
        listed, archived, draft, unlisted, scheduled, unknown
    }

    @GSerializable
    public static class File {
        public Hashes hashes;
        public String url;
        public String filename;
        public Boolean primary;
        public Integer size;
        @Nullable public Type file_type;

        @GSerializable
        public static class Hashes {
            public String sha512;
            public String sha1;
        }

        public enum Type {
            @SerializedName("required-resource-pack") REQUIRED_RESOURCE_PACK,
            @SerializedName("optional-resource-pack") OPTIONAL_RESOURCE_PACK
        }
    }
}
