package io.gitlab.jfronny.resclone.data;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

@GSerializable(generateAdapter = true)
public record PackMetaUnloaded(String fetcher, String source, String name, boolean forceDownload, boolean forceEnable) {
}
