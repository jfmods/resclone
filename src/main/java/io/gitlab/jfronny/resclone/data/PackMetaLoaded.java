package io.gitlab.jfronny.resclone.data;

import java.nio.file.Path;

public record PackMetaLoaded(Path zipPath, String name, boolean forceEnable) {
}
