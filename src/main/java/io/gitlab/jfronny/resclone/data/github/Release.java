package io.gitlab.jfronny.resclone.data.github;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.List;

@GSerializable
public class Release {
    public List<Asset> assets;
    public String zipball_url;

    @GSerializable
    public static class Asset {
        public String name;
        public String content_type;
        public String browser_download_url;
    }
}
