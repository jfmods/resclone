package io.gitlab.jfronny.resclone.util;

import io.gitlab.jfronny.resclone.Resclone;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class PackUrlCache {
    private final Path file;
    Properties properties = new Properties();

    public PackUrlCache(Path file) {
        this.file = file;
        if (Files.exists(file)) {
            try (BufferedReader r = Files.newBufferedReader(file)) {
                properties.load(r);
            } catch (IOException e) {
                Resclone.LOGGER.error("Could not load pack URL cache");
            }
        }
    }

    public void save() {
        try (BufferedWriter w = Files.newBufferedWriter(file)) {
            properties.store(w, "This is an internal file used for offline pack loading, do not edit");
        } catch (IOException e) {
            Resclone.LOGGER.error("Could not write pack URL cache");
        }
    }

    public boolean containsKey(String key) {
        return properties.containsKey(key);
    }

    public String get(String key) {
        return properties.getProperty(key);
    }

    public void set(String key, String value) {
        properties.setProperty(key, value);
    }
}
