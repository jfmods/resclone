package io.gitlab.jfronny.resclone.util.io;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.Predicate;

public class PathPruneVisitor extends SimpleFileVisitor<Path> {
    private final Predicate<Path> removalSelector;

    public PathPruneVisitor(Predicate<Path> removalSelector) {
        this.removalSelector = removalSelector;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (removalSelector.test(file)) Files.delete(file);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if (removalSelector.test(dir)) Files.walkFileTree(dir, new RemoveDirVisitor());
        return super.postVisitDirectory(dir, exc);
    }
}
