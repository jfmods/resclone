package io.gitlab.jfronny.resclone.util;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.throwable.ThrowingFunction;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ListAdaptation {
    public static <T, TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> List<T> deserializeList(Reader reader, ThrowingFunction<Reader, T, TEx> deserializeOne) throws TEx {
        List<T> result = new ArrayList<>();
        reader.beginArray();
        while (reader.hasNext()) {
            result.add(deserializeOne.apply(reader));
        }
        reader.endArray();
        return result;
    }

    public static <T, TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> Set<T> deserializeSet(Reader reader, ThrowingFunction<Reader, T, TEx> serializeOne) throws TEx {
        Set<T> result = new LinkedHashSet<>();
        reader.beginArray();
        while (reader.hasNext()) {
            result.add(serializeOne.apply(reader));
        }
        reader.endArray();
        return result;
    }
}
