package io.gitlab.jfronny.resclone.processors;

import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.RescloneConfig;
import io.gitlab.jfronny.resclone.util.io.PathPruneVisitor;

import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.util.Set;

public class PruneClutterProcessor implements PackProcessor {
    private final Set<String> clutterFiles = Set.of(".DS_Store", "Thumbs.db");

    @Override
    public void process(FileSystem p) throws Exception {
        Files.walkFileTree(p.getPath("."), new PathPruneVisitor(s -> {
            if (Files.isDirectory(s)) {
                return false;
            }
            if (clutterFiles.contains(s.getFileName().toString())) {
                if (RescloneConfig.logProcessing) Resclone.LOGGER.info("Pruning clutter: {0}", s);
                return true;
            }
            return false;
        }));
    }
}
