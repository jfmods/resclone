package io.gitlab.jfronny.resclone.processors;

import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.RescloneConfig;
import io.gitlab.jfronny.resclone.util.io.PathPruneVisitor;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.resource.*;
import net.minecraft.util.PathUtil;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.util.stream.Stream;

public class PruneVanillaProcessor implements PackProcessor {
    public static DefaultResourcePack referencePack = null; // Set from MinecraftClientMixin
    private static DefaultResourcePack vanillaDataPackProvider = null;

    @Override
    public void process(FileSystem p) throws Exception {
        try {
            if (referencePack == null && FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT) throw new Exception("Reference pack not set");
            if (vanillaDataPackProvider == null) vanillaDataPackProvider = VanillaDataPackProvider.createDefaultPack();
            if (Files.isDirectory(p.getPath("/assets/minecraft"))) {
                Files.walkFileTree(p.getPath("/assets/minecraft"), new PathPruneVisitor(s -> {
                    if (Files.isDirectory(s))
                        return false;
                    try {
                        String[] parts = p.getPath("/").relativize(s).toString().split("/");
                        if (!Stream.of(parts).allMatch(PathUtil::isFileNameValid)) return false;
                        InputSupplier<InputStream> vn = referencePack == null ? null : referencePack.openRoot(parts);
                        if (vn == null) vn = vanillaDataPackProvider.openRoot(parts);
                        if (vn != null) {
                            try (InputStream vns = vn.get(); InputStream pk = Files.newInputStream(s, StandardOpenOption.READ)) {
                                if (IOUtils.contentEquals(vns, pk)) {
                                    if (RescloneConfig.logProcessing) Resclone.LOGGER.info("Pruning file unchanged from vanilla: {0}", s);
                                    return true;
                                }
                            }
                        }
                    } catch (Throwable e) {
                        Resclone.LOGGER.error("Could not prune unchanged assets", e);
                    }
                    return false;
                }));
            }
        } catch (IOException e) {
            throw new Exception("Could not prune vanilla files", e);
        }
    }
}
