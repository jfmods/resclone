package io.gitlab.jfronny.resclone.processors;

import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.RescloneConfig;
import io.gitlab.jfronny.resclone.util.io.PathPruneVisitor;

import java.io.IOException;
import java.nio.file.*;

public class RemoveEmptyProcessor implements PackProcessor {
    @Override
    public void process(FileSystem p) throws Exception {
        if (Files.exists(p.getPath("/assets"))) {
            try {
                Files.walkFileTree(p.getPath("/assets"), new PathPruneVisitor(s -> {
                    if (Files.isDirectory(s)) {
                        try (DirectoryStream<Path> paths = Files.newDirectoryStream(s)) {
                            if (!paths.iterator().hasNext()) {
                                if (RescloneConfig.logProcessing)  Resclone.LOGGER.info("Pruning empty directory: {0}", s);
                                return true;
                            }
                        } catch (IOException e) {
                            Resclone.LOGGER.error("Could not check whether directory has entries", e);
                        }
                    }
                    return false;
                }));
            } catch (Throwable e) {
                throw new Exception("Failed to prune empty directories", e);
            }
        }
    }
}
