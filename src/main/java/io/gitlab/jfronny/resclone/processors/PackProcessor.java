package io.gitlab.jfronny.resclone.processors;

import java.nio.file.FileSystem;

public interface PackProcessor {
    void process(FileSystem p) throws Exception;
}
