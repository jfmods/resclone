package io.gitlab.jfronny.resclone.processors;

import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.RescloneConfig;
import io.gitlab.jfronny.resclone.util.io.MoveDirVisitor;

import java.io.IOException;
import java.nio.file.*;

public class RootPathProcessor implements PackProcessor {
    @Override
    public void process(FileSystem p) throws Exception {
        if (!Files.exists(p.getPath("/pack.mcmeta"))) {
            try {
                Path root = p.getPath("/");
                try (DirectoryStream<Path> paths = Files.newDirectoryStream(root)) {
                    for (Path path : paths) {
                        if (Files.isDirectory(path) && Files.exists(path.resolve("pack.mcmeta"))) {
                            if (RescloneConfig.logProcessing)  Resclone.LOGGER.info("Moving discovered root out of: {0}", path);
                            Files.walkFileTree(path, new MoveDirVisitor(path, root, StandardCopyOption.REPLACE_EXISTING));
                        }
                    }
                }
            } catch (IOException e) {
                throw new Exception("Could not fix root path", e);
            }
        }
    }
}
