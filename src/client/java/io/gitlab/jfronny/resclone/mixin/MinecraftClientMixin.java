package io.gitlab.jfronny.resclone.mixin;

import io.gitlab.jfronny.resclone.processors.PruneVanillaProcessor;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.resource.DefaultClientResourcePackProvider;
import net.minecraft.resource.DefaultResourcePack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(MinecraftClient.class)
public class MinecraftClientMixin {
    @Redirect(method = "<init>", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/resource/DefaultClientResourcePackProvider;getResourcePack()Lnet/minecraft/resource/DefaultResourcePack;"))
    DefaultResourcePack ae(DefaultClientResourcePackProvider instance) {
        DefaultResourcePack drp = instance.getResourcePack();
        PruneVanillaProcessor.referencePack = drp;
        return drp;
    }
}
