package io.gitlab.jfronny.resclone.mixin;

import io.gitlab.jfronny.resclone.Resclone;
import io.gitlab.jfronny.resclone.data.PackMetaLoaded;
import net.minecraft.client.option.GameOptions;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;

@Mixin(GameOptions.class)
public abstract class GameOptionsMixin {
    @Shadow public List<String> resourcePacks;

    @Shadow public abstract void write();

    @Inject(at = @At("TAIL"), method = "load()V")
    public void load(CallbackInfo ci) {
        for (PackMetaLoaded meta : Resclone.NEW_PACKS) {
            Resclone.LOGGER.info(Resclone.MOD_ID + "/" + meta.name());
            resourcePacks.add(Resclone.MOD_ID + "/" + meta.name());
        }
        if (!Resclone.NEW_PACKS.isEmpty())
            write();
        Resclone.NEW_PACKS.clear();
    }
}
